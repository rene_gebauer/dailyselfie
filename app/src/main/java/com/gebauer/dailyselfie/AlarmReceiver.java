package com.gebauer.dailyselfie;

/**
 * Created by renegebauer on 19/11/17.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class AlarmReceiver extends BroadcastReceiver {

    private static final String TAG = "LOCAL PUSH";
    private static String NOTIFICATION_CHANNEL = "my_channel_01";

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d(TAG, "AlarmReceiver onReceive() ");

        try {
            String title, message;

            Bundle b = intent.getExtras();
            title=b.getString("title");
            message=b.getString("message");

            Log.d(TAG, "AlarmReceiver title=" + title + " message=" + message);

            NotificationManager notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder mBuilder =
                    new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.ic_launcher)
                            .setContentTitle(title)
                            .setContentText(message);


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL,
                        title,
                        NotificationManager.IMPORTANCE_DEFAULT);

                channel.setDescription(context.getResources().getString(R.string.notif_title));
                notificationManager.createNotificationChannel(channel);

                mBuilder.setChannelId(NOTIFICATION_CHANNEL);
            }

			Intent resultIntent =  new Intent(context, MyActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            mBuilder.setAutoCancel(true);

            Notification notification = mBuilder.build();
	    	notificationManager.notify(message.hashCode(), notification);

        } catch (Exception e) {
            Log.e(TAG, "onReceive ERROR" +  e.toString());
        }
    }
}
