package com.gebauer.dailyselfie;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.Calendar;

public class MyActivity extends AppCompatActivity {

    static final String TAG = "MyActivity";
    static final int REQUEST_TAKE_PHOTO = 1;

    public static final String AUTHORITY = "com.gebauer.dailyselfie.photocontentprovider";
    public static final Uri BASE_URI = Uri.parse("content://" + AUTHORITY + "/");
    public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_URI, DbSchema.TBL_PHOTOS);

    public static String ALARM_ACTION = "ALARM_ACTION";
    public static int REPEATING_TIME = 120000; //30seconds


    PhotoListAdapter mAdapter;
    RecyclerView mRecycleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String[] projection = new String[]{DbSchema.COL_ID, DbSchema.COL_TITLE, DbSchema.COL_BITMAP};
        Cursor cursor =  getContentResolver().query(CONTENT_URI, projection,null,null,null);

        mRecycleView = findViewById(R.id.list_view);
        LinearLayoutManager layout =  new LinearLayoutManager(this);
        mRecycleView.setLayoutManager(layout);

        mAdapter = new PhotoListAdapter(this, cursor);
        mRecycleView.setAdapter(mAdapter);

        mRecycleView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, mRecycleView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Cursor c = (Cursor)mAdapter.getItem(position);
                        PhotoItem item =  PhotoItem.fromCursor(c);

                        Intent intent = new Intent(MyActivity.this, DisplayPhotoActivity.class);
                        intent.putExtra("title", item.getTitle());
                        intent.putExtra("blob",item.getBlob());
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position)
                    {
                        //TODO Maybe delete item?
                    }
                })
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {

        super.onPause();

        scheduleLocalNotifications(getResources().getString(R.string.notif_title), getResources().getString(R.string.notif_text));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.camera) {
            dispatchTakePictureIntent();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK)
        {
            Log.i(TAG, "onActivityResult ok");
            PhotoItem item = PhotoItem.fromIntent(data);
            mAdapter.saveItem(item);

            Log.i(TAG, "onActivityResult done" + item.getTitle());
        }
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
        }
    }

    private void scheduleLocalNotifications(String title, String message) {

        if (message.isEmpty() || title.isEmpty())
            return;

        AlarmReceiver receiver = new AlarmReceiver();
        IntentFilter filter = new IntentFilter(ALARM_ACTION);
        this.registerReceiver(receiver, filter);

        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent  = new Intent(this, AlarmReceiver.class);

        Bundle c = new Bundle();
        c.putString("title", title);
        c.putString("message", message);
        intent.putExtras(c);

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, message.hashCode(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP
                ,  Calendar.getInstance().getTimeInMillis() + REPEATING_TIME
                , REPEATING_TIME
                , pendingIntent);

        Log.i(TAG, "scheduleLocalNotifications done.");
    }
}
