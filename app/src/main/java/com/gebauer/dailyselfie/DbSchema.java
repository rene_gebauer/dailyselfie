package com.gebauer.dailyselfie;

import android.provider.BaseColumns;

/**
 * Created by renegebauer on 19/11/17.
 */


public interface DbSchema {

    String DB_NAME = "photos.db";

    String TBL_PHOTOS = "photos";

    String COL_ID = BaseColumns._ID;
    String COL_TITLE = "item_title";
    String COL_BITMAP = "item_bitmap";

    String DDL_CREATE_TBL_PHOTOS =
            "CREATE TABLE " + TBL_PHOTOS + " (" +
                    COL_ID + " INTEGER  PRIMARY KEY AUTOINCREMENT, \n" +
                    COL_TITLE + " TEXT,\n" +
                    COL_BITMAP + " BLOB \n" +
                    ")";

    String DDL_DROP_TBL_PHOTOS = "DROP TABLE IF EXISTS "+ TBL_PHOTOS;

}
