package com.gebauer.dailyselfie;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

public class DisplayPhotoActivity extends Activity {

	private static final String TAG = "DisplayPhotoActivity";
	private ImageView mBitmap;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.image);

		String title = getIntent().getStringExtra("title");
		byte[] blob = getIntent().getByteArrayExtra("blob");
		Bitmap bitmap = BitmapFactory.decodeByteArray(blob, 0, blob.length);

		mBitmap = findViewById(R.id.bitmap);
		mBitmap.setImageBitmap(bitmap);
	}

}
