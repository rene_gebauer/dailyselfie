package com.gebauer.dailyselfie;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;


//source: https://inducesmile.com/android-tips/android-recyclerview-inside-nestedscrollview-example/
//source: https://stackoverflow.com/questions/26517855/using-the-recyclerview-with-a-database
//source: https://developer.android.com

public class PhotoListAdapter extends RecyclerView.Adapter<PhotoItemHolder> {

    private static final String TAG = PhotoListAdapter.class.getSimpleName();

	private final Context mContext;
    private LayoutInflater mInflater;
    CursorAdapter mCursorAdapter;

    private static class ViewHolder  {
        ImageView imageView;
        TextView textView;
    }

	public PhotoListAdapter(Context context, Cursor c) {
		mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mCursorAdapter = new CursorAdapter(mContext, c, 0) {

            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                Log.i(TAG, "newView started " );
                View view = mInflater.inflate(R.layout.item, parent, false);

                ViewHolder holder = new ViewHolder();
                holder.imageView = view.findViewById(R.id.bitmap);
                holder.textView = view.findViewById(R.id.text);
                view.setTag(holder);

                return view;
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {

                int pos = cursor.getPosition();
                Log.i(TAG, "bindView started " + pos + "/" + cursor.getCount() );
                ViewHolder holder =  (ViewHolder) view.getTag();

                byte[] blob = cursor.getBlob(cursor.getColumnIndexOrThrow(DbSchema.COL_BITMAP));
                setPic(holder.imageView, blob);

                String title = cursor.getString(cursor.getColumnIndexOrThrow(DbSchema.COL_TITLE));
                holder.textView.setText(title);

                Log.i(TAG, "bindView done." );
            }
        };
    }

    @Override
    public PhotoItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mCursorAdapter.newView(mContext, mCursorAdapter.getCursor(), parent);
        return new PhotoItemHolder(view);
    }

    @Override
    public void onBindViewHolder(PhotoItemHolder holder, int position) {
        mCursorAdapter.getCursor().moveToPosition(position);
        mCursorAdapter.bindView(holder.itemView, mContext, mCursorAdapter.getCursor());
    }

    @Override
    public int getItemCount() {
        return  mCursorAdapter.getCount();
    }

    public void changeCursor(Cursor cursor) {
        mCursorAdapter.changeCursor(cursor);
        notifyDataSetChanged();
    }

    public Object getItem(int position) {
        return mCursorAdapter.getItem(position);
    }

    public void saveItem(PhotoItem item) {

        Uri uri = saveImage(item);
        if (uri != null) {
            Log.d(TAG, uri.getPath() + " saved.");
        }

        Log.d(TAG, item.getTitle() + " added.");

        String[] projection = new String[]{DbSchema.COL_ID, DbSchema.COL_TITLE, DbSchema.COL_BITMAP};
        Cursor cursor =  mContext.getContentResolver().query(MyActivity.CONTENT_URI, projection,null,null,null);
        changeCursor(cursor);

        mCursorAdapter.notifyDataSetChanged();
    }

    private Uri saveImage(PhotoItem item) {

        ContentValues values = new ContentValues();
        values.put(DbSchema.COL_TITLE, item.getTitle());
        values.put(DbSchema.COL_BITMAP, item.getBlob());

        Uri uri = mContext.getContentResolver().insert(MyActivity.CONTENT_URI, values );

        Log.d(TAG, "item " + item.getTitle() + " saved. URI: " + uri.getPath());

        return uri;
    }


    private void setPic(ImageView imageView, byte[] blob) {

	    Log.d(TAG, "setPic image length=" + blob.length);

        int targetW = imageView.getLayoutParams().width;
        int targetH = imageView.getLayoutParams().height;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(blob, 0, blob.length,  bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeByteArray(blob, 0, blob.length, bmOptions);
        imageView.setImageBitmap(bitmap);
    }
}
