package com.gebauer.dailyselfie;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public final class PhotoItemHolder extends RecyclerView.ViewHolder{

	public ImageView mImageView;
	public TextView mTextView;

	public PhotoItemHolder(View itemView) {
		super(itemView);

		mImageView = itemView.findViewById(R.id.bitmap);
		mTextView = itemView.findViewById(R.id.text);
	}
}
