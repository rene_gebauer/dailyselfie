package com.gebauer.dailyselfie;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;

public final class PhotoItem {

	public static final SimpleDateFormat FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss", Locale.US);

	private int mID;
	private String mTitle;
	private byte[] mBlob;

	public static PhotoItem fromCursor(Cursor cursor) {
		PhotoItem item = new PhotoItem();
        item.setId(cursor.getInt(cursor.getColumnIndex(DbSchema.COL_ID)));
		item.setTitle(cursor.getString(cursor.getColumnIndex(DbSchema.COL_TITLE)));
		item.setBlob(cursor.getBlob(cursor.getColumnIndex(DbSchema.COL_BITMAP)));
		return item;
	}

    public static PhotoItem fromIntent(Intent intent) {
        Bitmap bmp = (Bitmap)intent.getExtras().get("data");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] blob = stream.toByteArray();

        PhotoItem item = new PhotoItem();
        item.setTitle(FORMAT.format(new Date()));
        item.setBlob(blob);
        return item;
    }

	public int getId() {
		return mID;
	}
	public void setId(int id) {
		this.mID = id;
	}

	public String getTitle() {
		return mTitle;
	}
	public void setTitle(String title) {
		this.mTitle = title;
	}

	public byte[] getBlob() {
		return mBlob;
	}
	public void setBlob(byte[] blob) {
		this.mBlob = blob;
	}

}
